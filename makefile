all:
	$(MAKE) grammar
	$(MAKE) lex
	$(MAKE) compile_grammar
	$(MAKE) package_grammar
	$(MAKE) compile_code

grammar:
	bison -d grammar.y
	mv grammar.tab.h y.tab.h

lex:
	flex lex.l

compile_grammar:
	gcc -c grammar.tab.c lex.yy.c
package_grammar:
	ar rvs lexgram.a grammar.tab.o lex.yy.o
compile_code:
	gcc -lobjc -framework Foundation code/main.m lexgram.a

clean:
	rm *.tab.h *.tab.c *.tab.o *.yy.c *.yy.o *.a *.out 2> /dev/null