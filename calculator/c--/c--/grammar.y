%{
    #include <stdio.h>
    #include "Node.h"
    
    char *yyIdentifier;
    char *yyStringLiteral;
    char *yyEnumerationConstant;
    char *yyFConstant;
    char *yyIConstant;
    char *yyTypedefName;
%}

%union {
    struct node *node;
    char *str;
}

%token	IDENTIFIER I_CONSTANT F_CONSTANT STRING_LITERAL FUNC_NAME SIZEOF
%token	PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token	AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token	SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token	XOR_ASSIGN OR_ASSIGN
%token	TYPEDEF_NAME ENUMERATION_CONSTANT

%token	TYPEDEF EXTERN STATIC AUTO REGISTER INLINE
%token	CONST RESTRICT VOLATILE
%token	BOOL CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE VOID
%token	COMPLEX IMAGINARY 
%token	STRUCT UNION ENUM ELLIPSIS NAMESPACE PUBLIC

%token	CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%token	ALIGNAS ALIGNOF ATOMIC GENERIC NORETURN STATIC_ASSERT THREAD_LOCAL

%start file

%type<node> file translation_unit struct_declaration_list struct_declaration struct_declarator struct_declarator_list compound_statement block_item_list declarator direct_declarator pointer type_qualifier_list type_qualifier statement labeled_statement expression_statement selection_statement iteration_statement jump_statement constant_expression conditional_expression logical_or_expression logical_and_expression inclusive_or_expression exclusive_or_expression and_expression equality_expression relational_expression shift_expression additive_expression multiplicative_expression cast_expression unary_expression specifier_qualifier_list type_specifier atomic_type_specifier enum_specifier  enumerator_list enumerator enumeration_constant struct_or_union_specifier struct_or_union static_assert_declaration unary_operator postfix_expression primary_expression identifier_list constant string generic_selection assignment_expression generic_assoc_list expression generic_association assignment_operator block_item declaration type_name abstract_declarator direct_abstract_declarator parameter_type_list parameter_list parameter_declaration declaration_specifiers storage_class_specifier function_specifier alignment_specifier init_declarator_list init_declarator initializer initializer_list designation designator_list designator argument_expression_list declaration_list function_definition external_declaration
//%type<str> IDENTIFIER
%%

primary_expression
    : IDENTIFIER            { $$ = mknode(0,0,yyIdentifier); }
	| constant              { $$ = $1; }
	| string                { $$ = $1; }
	| '(' expression ')'    { $$ = mknode(0,mknode($2,0,")"),"("); }
	| generic_selection     { $$ = $1; }
	;

constant
    : I_CONSTANT		/* includes character_constant */           { $$ = mknode(0,0,yyIConstant); }
	| F_CONSTANT                                                    { $$ = mknode(0,0,yyFConstant); }
	| ENUMERATION_CONSTANT	/* after it has been defined as such */ { $$ = mknode(0,0,yyEnumerationConstant); }
	;

enumeration_constant		/* before it has been defined as such */
    : IDENTIFIER                { $$ = mknode(0,0,yyIdentifier); }
	;

string
	: STRING_LITERAL            { $$ = mknode(0,0,yyStringLiteral); }
	| FUNC_NAME                 { $$ = mknode(0,0,"__func__"); }
	;

generic_selection
    : GENERIC '(' assignment_expression ',' generic_assoc_list ')'   { $$ = mknode(0,mknode(0,mknode(mknode($3,$5,","),0,")"),"("),"_Generic"); }
	;

generic_assoc_list
    : generic_association                           { $$ = $1; }
    | generic_assoc_list ',' generic_association    { $$ = mknode($1,$3,","); }
	;

generic_association
    : type_name ':' assignment_expression           { $$ = mknode($1,$3,":"); }
	| DEFAULT ':' assignment_expression             { $$ = mknode(mknode(0,0,"default"),$3,":"); }
	;

postfix_expression
	: primary_expression                                    { $$ = $1; }
	| postfix_expression '[' expression ']'                 { $$ = mknode($1,mknode($3,0,"]"),"["); }
    | postfix_expression '(' ')'                            { $$ = mknode($1,0,"( )"); }
	| postfix_expression '(' argument_expression_list ')'   { $$ = mknode($1,mknode($3,0,")"),"("); }
	| postfix_expression '.' IDENTIFIER                     { $$ = mknode($1,mknode(0,0,yyIdentifier),"."); }
	| postfix_expression PTR_OP IDENTIFIER                  { $$ = mknode($1,mknode(0,0,yyIdentifier),"->"); }
	| postfix_expression INC_OP                             { $$ = mknode($1,0,"++"); }
	| postfix_expression DEC_OP                             { $$ = mknode($1,0,"--"); }
	| '(' type_name ')' '{' initializer_list '}'            { $$ = mknode(mknode(0,mknode($2,0,")"),"("),mknode(0,mknode($5,0,"}"),"{"),""); }
	| '(' type_name ')' '{' initializer_list ',' '}'        { $$ = mknode(mknode(0,mknode($2,0,")"),"("),mknode(0,mknode(mknode($5,mknode(0,0,","),""),0,"}"),"{"),""); }
	;

argument_expression_list
    : assignment_expression                                 { $$ = $1; }
    | argument_expression_list ',' assignment_expression    { $$ = mknode($1,$3,","); }
	;

unary_expression
	: postfix_expression                    { $$ = $1; }
	| INC_OP unary_expression               { $$ = mknode(0,$2,"++"); }
	| DEC_OP unary_expression               { $$ = mknode(0,$2,"--"); }
	| unary_operator cast_expression        { $$ = mknode($1,$2,""); }
	| SIZEOF unary_expression               { $$ = mknode(0,$2,"sizeof"); }
    | SIZEOF '(' type_name ')'              { $$ = mknode(0,mknode(0,mknode($3,0,")"),"("),"sizeof"); }
	| ALIGNOF '(' type_name ')'             { $$ = mknode(0,mknode(0,mknode($3,0,")"),"("),"_Alignof"); }
	;

unary_operator
	: '&'                                                               { $$ = mknode(0,0,"&"); }
	| '*'                                                               { $$ = mknode(0,0,"*"); }
	| '+'                                                               { $$ = mknode(0,0,"+"); }
	| '-'                                                               { $$ = mknode(0,0,"-"); }
	| '~'                                                               { $$ = mknode(0,0,"~"); }
	| '!'                                                               { $$ = mknode(0,0,"!"); }
	;

cast_expression
	: unary_expression                                                  { $$ = $1; }
	| '(' type_name ')' cast_expression                                 { $$ = mknode(mknode(0,$2,")"),$4,")"); }
	;

multiplicative_expression
	: cast_expression                                                   { $$ = $1; }
	| multiplicative_expression '*' cast_expression                     { $$ = mknode($1,$3,"*"); }
	| multiplicative_expression '/' cast_expression                     { $$ = mknode($1,$3,"/"); }
	| multiplicative_expression '%' cast_expression                     { $$ = mknode($1,$3,"%"); }
	;

additive_expression
	: multiplicative_expression                                         { $$ = $1; }
	| additive_expression '+' multiplicative_expression                 { $$ = mknode($1,$3,"+"); }
	| additive_expression '-' multiplicative_expression                 { $$ = mknode($1,$3,"-"); }
	;

shift_expression
	: additive_expression                                               { $$ = $1; }
	| shift_expression LEFT_OP additive_expression                      { $$ = mknode($1,$3,"<<"); }
	| shift_expression RIGHT_OP additive_expression                     { $$ = mknode($1,$3,">>"); }
	;

relational_expression
	: shift_expression                                                  { $$ = $1; }
	| relational_expression '<' shift_expression                        { $$ = mknode($1,$3,"<"); }
	| relational_expression '>' shift_expression                        { $$ = mknode($1,$3,">"); }
	| relational_expression LE_OP shift_expression                      { $$ = mknode($1,$3,"<="); }
	| relational_expression GE_OP shift_expression                      { $$ = mknode($1,$3,">="); }
	;

equality_expression
	: relational_expression                                             { $$ = $1; }
	| equality_expression EQ_OP relational_expression                   { $$ = mknode($1,$3,"=="); }
	| equality_expression NE_OP relational_expression                   { $$ = mknode($1,$3,"!="); }
	;

and_expression
	: equality_expression                                               { $$ = $1; }
	| and_expression '&' equality_expression                            { $$ = mknode($1,$3,"&"); }
	;

exclusive_or_expression
	: and_expression                                                    { $$ = $1; }
	| exclusive_or_expression '^' and_expression                        { $$ = mknode($1,$3,"^"); }
	;

inclusive_or_expression
	: exclusive_or_expression                                           { $$ = $1; }
	| inclusive_or_expression '|' exclusive_or_expression               { $$ = mknode($1,$3,"|"); }
	;

logical_and_expression
	: inclusive_or_expression                                           { $$ = $1; }
    | logical_and_expression AND_OP inclusive_or_expression             { $$ = mknode($1,$3,"&&"); }
	;

logical_or_expression
	: logical_and_expression                                            { $$ = $1; }
    | logical_or_expression OR_OP logical_and_expression                { $$ = mknode($1,$3,"||"); }
	;

conditional_expression
    : logical_or_expression                                             { $$ = $1; }
	| logical_or_expression '?' expression ':' conditional_expression   { $$ = mknode($1,mknode($3,$5,":"),"?"); }
	;

assignment_expression
    : conditional_expression                                            { $$ = $1; }
    | unary_expression assignment_operator assignment_expression        { $$ = mknode($1,mknode($2,$3,""),""); }
	;

assignment_operator
	: '='                   { $$ = mknode(0,0,"="); }
	| MUL_ASSIGN            { $$ = mknode(0,0,"*="); }
	| DIV_ASSIGN            { $$ = mknode(0,0,"/="); }
	| MOD_ASSIGN            { $$ = mknode(0,0,"%="); }
	| ADD_ASSIGN            { $$ = mknode(0,0,"+="); }
	| SUB_ASSIGN            { $$ = mknode(0,0,"-="); }
	| LEFT_ASSIGN           { $$ = mknode(0,0,"<<="); }
	| RIGHT_ASSIGN          { $$ = mknode(0,0,">>="); }
	| AND_ASSIGN            { $$ = mknode(0,0,"&="); }
	| XOR_ASSIGN            { $$ = mknode(0,0,"^="); }
	| OR_ASSIGN             { $$ = mknode(0,0,"|="); }
	;

expression
    : assignment_expression                     { $$ = $1; }
    | expression ',' assignment_expression      { $$ = mknode($1,$3,","); }
	;

constant_expression
    : conditional_expression	/* with constraints */ { $$ = $1; }
	;

declaration
    : declaration_specifiers ';'                        { $$ = mknode($1,0,";"); }
	| declaration_specifiers init_declarator_list ';'   { $$ = mknode(mknode($1,$2,""),0,";"); }
	| static_assert_declaration                         { $$ = $1; }
	;

declaration_specifiers
    : storage_class_specifier declaration_specifiers    { $$ = mknode($1,$2,""); }
    | storage_class_specifier                           { $$ = $1; }
	| type_specifier declaration_specifiers             { $$ = mknode($1,$2,""); }
	| type_specifier                                    { $$ = $1; }
	| type_qualifier declaration_specifiers             { $$ = mknode($1,$2,""); }
	| type_qualifier                                    { $$ = $1; }
	| function_specifier declaration_specifiers         { $$ = mknode($1,$2,""); }
	| function_specifier                                { $$ = $1; }
	| alignment_specifier declaration_specifiers        { $$ = mknode($1,$2,""); }
	| alignment_specifier                               { $$ = $1; }
	;

init_declarator_list
    : init_declarator                           { $$ = $1; }
    | init_declarator_list ',' init_declarator  { $$ = mknode($1,$3,","); }
	;

init_declarator
	: declarator '=' initializer                { $$ = mknode($1,$3,"="); }
	| declarator                                { $$ = $1; }
	;

storage_class_specifier
    : TYPEDEF	/* identifiers must be flagged as TYPEDEF_NAME */   { $$ = mknode(0,0,"typedef"); }
	| EXTERN                                                        { $$ = mknode(0,0,"extern"); }
	| STATIC                                                        { $$ = mknode(0,0,"static"); }
	| THREAD_LOCAL                                                  { $$ = mknode(0,0,"_Thread_local"); }
	| AUTO                                                          { $$ = mknode(0,0,"auto"); }
	| REGISTER                                                      { $$ = mknode(0,0,"register"); }
	;

type_specifier
	: VOID                                                              { $$ = mknode(0,0,"void"); }
	| CHAR                                                              { $$ = mknode(0,0,"char"); }
	| SHORT                                                             { $$ = mknode(0,0,"short"); }
	| INT                                                               { $$ = mknode(0,0,"int"); }
	| LONG                                                              { $$ = mknode(0,0,"long"); }
	| FLOAT                                                             { $$ = mknode(0,0,"float"); }
	| DOUBLE                                                            { $$ = mknode(0,0,"double"); }
	| SIGNED                                                            { $$ = mknode(0,0,"signed"); }
	| UNSIGNED                                                          { $$ = mknode(0,0,"unsigned"); }
	| BOOL                                                              { $$ = mknode(0,0,"_Bool"); }
	| COMPLEX                                                           { $$ = mknode(0,0,"_Complex"); }
	| IMAGINARY	  	/* non-mandated extension */                        { $$ = mknode(0,0,"_Imaginary"); }
    | atomic_type_specifier                                             { $$ = $1; }
	| struct_or_union_specifier                                         { $$ = $1; }
	| enum_specifier                                                    { $$ = $1; }
    | TYPEDEF_NAME /* after it has been defined as such */              { $$ = mknode(0,0,yyTypedefName); }
	;

struct_or_union_specifier
	: struct_or_union '{' struct_declaration_list '}'                   { $$ = mknode($1,mknode($3,0,"}"),"{"); }
	| struct_or_union IDENTIFIER '{' struct_declaration_list '}'        { $$ = mknode($1,mknode(mknode(0,0,yyIdentifier),mknode($4,0,"}"),"{"),""); }
	| struct_or_union IDENTIFIER                                        { $$ = mknode($1,mknode(0,0,yyIdentifier),""); }
	;

struct_or_union
	: STRUCT                                                            { $$ = mknode(0,0,"struct"); }
	| UNION                                                             { $$ = mknode(0,0,"union"); }
	;

struct_declaration_list
    : struct_declaration                                                { $$ = $1; }
	| struct_declaration_list struct_declaration                        { $$ = mknode($1,$2,""); }
	;

struct_declaration
	: specifier_qualifier_list ';'	/* for anonymous struct/union */    { $$ = mknode($1,mknode(0,0,";"),""); }
    | specifier_qualifier_list struct_declarator_list ';'               { $$ = mknode($1,mknode($2,mknode(0,0,";"),""),""); }
    | static_assert_declaration                                         { $$ = $1; }
	;

specifier_qualifier_list
	: type_specifier specifier_qualifier_list               { $$ = mknode($1,$2,"");}
	| type_specifier                                        { $$ = $1;}
	| type_qualifier specifier_qualifier_list               { $$ = mknode($1,$2,"");}
	| type_qualifier                                        { $$ = $1;}
	;

struct_declarator_list
	: struct_declarator                                     { $$ = $1;}
	| struct_declarator_list ',' struct_declarator          { $$ = mknode($1,$3,",");}
	;

struct_declarator
	: ':' constant_expression                               { $$ = mknode(0,$2,":");}
	| declarator ':' constant_expression                    { $$ = mknode($1,$3,":");}
	| declarator                                            { $$ = $1; }
	;

enum_specifier
	: ENUM '{' enumerator_list '}'                          { $$ = mknode(0,mknode(0,mknode($3,0,"}"),"{"),"enum"); }
	| ENUM '{' enumerator_list ',' '}'                      { $$ = mknode(0,mknode(0,mknode(mknode($3,0,","),0,"}"),"{"),"enum"); }
	| ENUM IDENTIFIER '{' enumerator_list '}'               { $$ = mknode(0,mknode(mknode(0,0,yyIdentifier),mknode($4,0,"}"),"{"),"enum"); }
	| ENUM IDENTIFIER '{' enumerator_list ',' '}'           { $$ = mknode(0,mknode(mknode(0,0,yyIdentifier),mknode(mknode($4,0,","),0,"}"),"{"),"enum"); }
	| ENUM IDENTIFIER                                       { $$ = mknode(0,mknode(0,0,yyIdentifier),"enum"); }
	;

enumerator_list
	: enumerator                                            { $$ = $1; }
	| enumerator_list ',' enumerator                        { $$ = mknode($1,$3,","); }
	;

enumerator	/* identifiers must be flagged as ENUMERATION_CONSTANT */
	: enumeration_constant '=' constant_expression          { $$ = mknode($1,$3,"="); }
	| enumeration_constant                                  { $$ = $1; }
	;

atomic_type_specifier
	: ATOMIC '(' type_name ')'      { $$ = mknode(0,mknode(0,mknode($3,0,")"),"("),"_Atomic"); }
	;

type_qualifier
    : CONST            { $$ = mknode(0,0,"const"); }
	| RESTRICT         { $$ = mknode(0,0,"restrict"); }
	| VOLATILE         { $$ = mknode(0,0,"volatile"); }
	| ATOMIC           { $$ = mknode(0,0,"_Atomic"); }
	;

function_specifier
    : INLINE            { $$ = mknode(0,0,"inline"); }
	| NORETURN          { $$ = mknode(0,0,"_Noreturn"); }
	;

alignment_specifier
    : ALIGNAS '(' type_name ')'             { $$ = mknode(0,mknode(0,mknode($3,0,")"),"("),"_Alignas"); }
	| ALIGNAS '(' constant_expression ')'   { $$ = mknode(0,mknode(0,mknode($3,0,")"),"("),"_Alignas"); }
	;

declarator
    : pointer direct_declarator             { $$ = mknode($1,$2,""); }
    | direct_declarator                     { $$ = $1; }
	;

direct_declarator
	: IDENTIFIER                                                                    { $$ = mknode(0,0,yyIdentifier); }
	| '(' declarator ')'                                                            { $$ = mknode(0,mknode($2,0,")"),"("); }
	| direct_declarator '[' ']'                                                     { $$ = mknode($1,mknode(0,0,"[ ]"),""); }
    | direct_declarator '[' '*' ']'                                                 { $$ = mknode($1,mknode(0,0,"[ * ]"),""); }
    | direct_declarator '[' STATIC type_qualifier_list assignment_expression ']'    { $$ = mknode($1,mknode(0,mknode(mknode(0,mknode($4,$5,""),"static"),0,"]"),"["),""); }
	| direct_declarator '[' STATIC assignment_expression ']'                        { $$ = mknode($1,mknode(0,mknode(mknode(0,$4,"static"),0,"]"),"["),""); }
	| direct_declarator '[' type_qualifier_list '*' ']'                             { $$ = mknode($1,mknode(0,mknode(mknode($3,0,"*"),0,"]"),"["),""); }
	| direct_declarator '[' type_qualifier_list STATIC assignment_expression ']'    { $$ = mknode($1,mknode(0,mknode(mknode($3,$5,"static"),0,"]"),"["),""); }
	| direct_declarator '[' type_qualifier_list assignment_expression ']'           { $$ = mknode($1,mknode(0,mknode(mknode($3,$4,""),0,"]"),"["),""); }
	| direct_declarator '[' type_qualifier_list ']'                                 { $$ = mknode($1,mknode(0,mknode($3,0,"]"),"["),""); }
	| direct_declarator '[' assignment_expression ']'                               { $$ = mknode($1,mknode(0,mknode($3,0,"]"),"["),""); }
	| direct_declarator '(' parameter_type_list ')'                                 { $$ = mknode($1,mknode(0,mknode($3,0,")"),"("),""); }
	| direct_declarator '(' ')'                                                     { $$ = mknode($1,mknode(0,0,"( )"),""); }
	| direct_declarator '(' identifier_list ')'                                     { $$ = mknode($1,mknode(0,mknode($3,0,")"),"("),""); }
    ;

pointer
	: '*' type_qualifier_list pointer       { $$ = mknode(0,mknode($2,$3,""),"*"); }
	| '*' type_qualifier_list               { $$ = mknode(0,$2,"*"); }
	| '*' pointer                           { $$ = mknode(0,$2,"*"); }
    | '*'                                   { $$ = mknode(0,0,"*"); }
	;

type_qualifier_list
    : type_qualifier                        { $$ = $1; }
	| type_qualifier_list type_qualifier    { $$ = mknode($1,$2,""); }
	;


parameter_type_list
    : parameter_list ',' ELLIPSIS           { $$ = mknode($1,mknode(0,0,"..."),","); }
    | parameter_list                        { $$ = $1; }
	;

parameter_list
    : parameter_declaration                     { $$ = $1; }
    | parameter_list ',' parameter_declaration  { $$ = mknode($1,$3,","); }
	;

parameter_declaration
    : declaration_specifiers declarator             { $$ = mknode($1,$2,""); }
    | declaration_specifiers abstract_declarator    { $$ = mknode($1,$2,""); }
    | declaration_specifiers                        { $$ = $1; }
	;

identifier_list
    : IDENTIFIER                                            { $$ = mknode(0,0,yyIdentifier); }
    | identifier_list ',' IDENTIFIER                        { $$ = mknode($1,mknode(0,0,yyIdentifier),","); }
	;

type_name
	: specifier_qualifier_list abstract_declarator          { $$ = mknode($1,$2,""); }
	| specifier_qualifier_list                              { $$ = $1; }
	;

abstract_declarator
	: pointer direct_abstract_declarator            { $$ = mknode($1,$2,""); }
	| pointer                                       { $$ = $1; }
	| direct_abstract_declarator                    { $$ = $1; }
	;

direct_abstract_declarator
	: '(' abstract_declarator ')'                                                         { $$ = mknode(0,mknode($2,0,")"),"("); }
    | '[' ']'                                                                             { $$ = mknode(0,0,"[ ]"); }
	| '[' '*' ']'                                                                         { $$ = mknode(0,0,"[ * ]"); }
	| '[' STATIC type_qualifier_list assignment_expression ']'                            { $$ = mknode(0,mknode(mknode(0,mknode($3,$4,""),"static"),0,"]"),"["); }
	| '[' STATIC assignment_expression ']'                                                { $$ = mknode(0,mknode(mknode(0,$3,"static"),0,"]"),"["); }
	| '[' type_qualifier_list STATIC assignment_expression ']'                            { $$ = mknode(0,mknode(mknode($2,mknode($4,0,"static"),""),0,"]"),"["); }
	| '[' type_qualifier_list assignment_expression ']'                                   { $$ = mknode(0,mknode(mknode($2,$3,""),0,"]"),"["); }
	| '[' type_qualifier_list ']'                                                         { $$ = mknode(0,mknode($2,0,"]"),"["); }
	| '[' assignment_expression ']'                                                       { $$ = mknode(0,mknode($2,0,"]"),"["); }
	| direct_abstract_declarator '[' ']'                                                  { $$ = mknode($1,mknode(0,0,"[ ]"),""); }
	| direct_abstract_declarator '[' '*' ']'                                              { $$ = mknode($1,mknode(0,0,"[ * ]"),""); }
    | direct_abstract_declarator '[' STATIC type_qualifier_list assignment_expression ']' { $$ = mknode($1,mknode(0,mknode(mknode(0,mknode($4,$5,""),"static"),0,"]"),"["),""); }
	| direct_abstract_declarator '[' STATIC assignment_expression ']'                     { $$ = mknode($1,mknode(0,mknode(mknode(0,$4,"static"),0,"]"),"["),""); }
	| direct_abstract_declarator '[' type_qualifier_list assignment_expression ']'        { $$ = mknode($1,mknode(0,mknode(mknode($3,$4,""),0,"]"),"["),""); }
	| direct_abstract_declarator '[' type_qualifier_list STATIC assignment_expression ']' { $$ = mknode($1,mknode(0,mknode(mknode($3,mknode(0,$5,"static"),""),0,"]"),"["),""); }
	| direct_abstract_declarator '[' type_qualifier_list ']'                              { $$ = mknode($1,mknode(0,mknode($3,0,"]"),"["),""); }
	| direct_abstract_declarator '[' assignment_expression ']'                            { $$ = mknode($1,mknode(0,mknode($3,0,"]"),"["),""); }
	| '(' ')'                                                                             { $$ = mknode(0,0,"( )"); }
	| '(' parameter_type_list ')'                                                         { $$ = mknode(0,mknode($2,0,")"),"("); }
	| direct_abstract_declarator '(' ')'                                                  { $$ = mknode($1,mknode(0,0,"( )"),""); }
	| direct_abstract_declarator '(' parameter_type_list ')'                              { $$ = mknode($1,mknode(0,mknode($3,0,")"),"("),""); }
	;

initializer
    : '{' initializer_list '}'          { $$ = mknode(0,mknode($2,0,"}"),"{"); }
    | '{' initializer_list ',' '}'      { $$ = mknode(0,mknode(mknode($2,0,","),0,"}"),"{"); }
    | assignment_expression             { $$ = $1; }
	;

initializer_list
    : designation initializer                       { $$ = mknode($1,$2,""); }
    | initializer                                   { $$ = $1; }
    | initializer_list ',' designation initializer  { $$ = mknode($1,mknode($3,$4,""),","); }
	| initializer_list ',' initializer              { $$ = mknode($1,$3,","); }
	;

designation
    : designator_list '='           { $$ = mknode($1,0,"="); }
	;

designator_list
    : designator                    { $$ = $1; }
    | designator_list designator    { $$ = mknode($1,$2,""); }
	;

designator
    : '[' constant_expression ']'   { $$ = mknode(0,mknode($2,0,"]"),"["); }
    | '.' IDENTIFIER                { $$ = mknode(0,mknode(0,0,yyIdentifier),"."); }
	;

static_assert_declaration
    : STATIC_ASSERT '(' constant_expression ',' STRING_LITERAL ')' ';'   { $$ = mknode(0,mknode(0,mknode(mknode($3,mknode(0,0,yyStringLiteral),","),mknode(0,0,";"),")"),"("),"_Static_assert"); }
	;

statement
	: labeled_statement                             { $$ = $1; }
	| compound_statement                            { $$ = $1; }
	| expression_statement                          { $$ = $1; }
	| selection_statement                           { $$ = $1; }
	| iteration_statement                           { $$ = $1; }
	| jump_statement                                { $$ = $1; }
	;

labeled_statement
	: IDENTIFIER ':' statement                      { $$ = mknode(mknode(0,0,yyIdentifier),$3,":"); }
	| CASE constant_expression ':' statement        { $$ = mknode(mknode(0,$2,"case"),$4,":"); }
	| DEFAULT ':' statement                         { $$ = mknode(mknode(0,0,"default"),$3,":"); }
	;

compound_statement
	: '{' '}'                                       { $$ = mknode(0,0,"{ }"); }
	| '{'  block_item_list '}'                      { $$ = mknode(0,mknode(0,$2,"}"),"{"); }
	;

block_item_list
    : block_item                                    { $$ = $1; }
	| block_item_list block_item                    { $$ = mknode($1,$2,""); }
	;

block_item
	: declaration                                   { $$ = $1; }
	| statement                                     { $$ = $1; }
	;

expression_statement
    : ';'               { $$ = mknode(0,0,";"); }
	| expression ';'    { $$ = mknode($1,0,";"); }
	;

selection_statement
    : IF '(' expression ')' statement ELSE statement    { $$ = mknode(0,mknode(mknode(0,mknode($3,0,")"),"("),mknode($5,$7,"else"),""),"if"); }
	| IF '(' expression ')' statement                   { $$ = mknode(0,mknode(mknode(0,mknode($3,0,")"),"("),$5,""),"if"); }
    | SWITCH '(' expression ')' statement               { $$ = mknode(0,mknode(mknode(0,mknode($3,0,")"),"("),$5,""),"switch"); }
	;

iteration_statement
	: WHILE '(' expression ')' statement
	| DO statement WHILE '(' expression ')' ';'
	| FOR '(' expression_statement expression_statement ')' statement
	| FOR '(' expression_statement expression_statement expression ')' statement
	| FOR '(' declaration expression_statement ')' statement
	| FOR '(' declaration expression_statement expression ')' statement
	;

jump_statement
    : GOTO IDENTIFIER ';'       { $$ = mknode(0,mknode(0,mknode(0,0,";"),yyIdentifier),"goto"); }
	| CONTINUE ';'              { $$ = mknode(0,mknode(0,0,";"),"continue"); }
	| BREAK ';'                 { $$ = mknode(0,mknode(0,0,";"),"break"); }
	| RETURN ';'                { $$ = mknode(0,mknode(0,0,";"),"return"); }
    | RETURN expression ';'     { $$ = mknode(0,mknode($2,0,";"),"return"); }

	;

file
    : translation_unit                          { printtree($1); }
    ;

translation_unit
    : external_declaration                      { $$ = $1; }
	| translation_unit external_declaration     { $$ = mknode($1,$2,""); }
	;

external_declaration
    : function_definition           { $$ = $1; }
    | declaration                   { $$ = $1; }
	;

function_definition
    : declaration_specifiers declarator declaration_list compound_statement     { $$ = mknode($1,mknode($2,mknode($3,$4,""),""),""); }
	| declaration_specifiers declarator compound_statement                      { $$ = mknode($1,mknode($2,$3,""),""); }
	;

declaration_list
    : declaration                       { $$ = $1; }
    | declaration_list declaration      { $$ = mknode($1,$2,""); }
	;

%%