//
//  main.m
//  c--
//
//  Created by Mejdej on 15/06/18.
//  Copyright (c) 2018 Mejdej. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Node.h"

extern int yyparse (void);

int main(int argc, const char * argv[])
{
    if (argc > 1)
    {
        freopen(argv[1],"r",stdin);
    }
    else
    {
        printf("> ");
    }
    
    int code = yyparse();
    char *valid = code==0 ? "valid" : "invalid";
    printf("\n%s\nbye!\n", valid);
    return code;
}

int yyerror (char *s)
{
    fprintf (stderr, "%s\n", s);
    return -1;
}