//
//  Node.h
//  calc
//
//  Created by Mejdej on 15/06/18.
//  Copyright (c) 2018 Mejdej. All rights reserved.
//

#import <stdio.h>
#import <stdlib.h>
#import <string.h>

typedef struct node
{
    int i;
    struct node *left;
    struct node *right;
    char *token;
} node;

node *mknode(node *left, node *right, char *token);
void printtree(node *tree);
