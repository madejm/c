//
//  main.m
//  calc
//
//  Created by Mejdej on 15/06/18.
//  Copyright (c) 2018 Mejdej. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Node.h"

extern int yyparse (void);

int main(int argc, const char * argv[]) {
    
    printf("> ");
    return yyparse();
}

int yyerror (char *s)
{
    fprintf (stderr, "%s\n", s);
    return -1;
}