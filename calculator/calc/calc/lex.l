%{
    #include "y.tab.h"
    extern char *yystring;
%}

%option bison-bridge
%%
[0-9]+ {
    yystring = yytext;
    return NUMBER;
}
[\t\n] ;
"+" return(PLUS);
"-" return(MINUS);
"*" return(TIMES);
"/" return(DIVIDE);
"^" return(POWER);
"(" return(LEFT_PARENTHESIS);
")" return(RIGHT_PARENTHESIS);
";" return(END);
%%
int yywrap (void)
{
    return 1;
}