%{
    #include <stdio.h>
    #include "Node.h"
    
    char *yystring;
%}

%union {
    struct node *node;
    int val;
}

%start lines
%token NUMBER
%token PLUS MINUS TIMES DIVIDE POWER
%token LEFT_PARENTHESIS RIGHT_PARENTHESIS
%token END
%left  PLUS MINUS
%left  TIMES DIVIDE
%right POWER

%type<node> exp term factor
%type<val> NUMBER
%%
lines:  /* empty */
     | lines line /* do nothing */
     ;

line:
    exp END { printtree($1); printf("\n");}
    ;

exp: term               {$$ = $1;}
    | exp PLUS term     {$$ = mknode($1, $3, "+");}
    | exp MINUS term    {$$ = mknode($1, $3, "-");}
    ;

term: factor            {$$ = $1;}
    | term TIMES factor {$$ = mknode($1, $3, "*");}
    ;

factor: NUMBER         {$$ = mknode(0,0,yystring);}
      | LEFT_PARENTHESIS exp RIGHT_PARENTHESIS {$$ = $2;}
      ;
%%
