//
//  Node.m
//  calc
//
//  Created by Mejdej on 15/06/18.
//  Copyright (c) 2018 Mejdej. All rights reserved.
//

#import "Node.h"
#import <Foundation/Foundation.h>

extern char *yystring;

char *p_node(node *node)
{
    if (node == nil)
        return "nil";
    return node->token;
}

node *mknode(node *left, node *right, char *token)
{
    printf("mknode(%s, %s, %s)\n", p_node(left), p_node(right), token);
    /* malloc the node */
    node *newnode = (node *)malloc(sizeof(struct node));
    unsigned long length = strlen(token);
    char *newstr = (char *)malloc(length+1);
    strcpy(newstr, token);
    newnode->left = left;
    newnode->right = right;
    newnode->token = newstr;
    return newnode;
}

void printtree(node *tree)
{
    if (tree->left || tree->right)
        printf("(");
    
    if (tree->left)
        printtree(tree->left);
    
    printf(" %s ", tree->token);
    
    if (tree->right)
        printtree(tree->right);
    
    if (tree->left || tree->right)
        printf(")");
}
