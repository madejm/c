
class Car {
	public:
	int speed;
	float distance;
	int honks;

	Car();
	void honk();
};

Car::Car() {
	this->speed = 10;
	this->distance = 16583.71f;
	this->honks = 0;
}

void Car::honk() {
	printf("HONK! (%d)\n", this->honks); 
	this->honks++;
}

int main() {

	Car myCar;

	printf("Speed %d\n", myCar.speed);
	printf("Distance %.2f\n", myCar.distance);
	myCar.honk();
	myCar.honk();
	myCar.honk();
	return 0;
}