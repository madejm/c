
struct Car {

	int speed;
	float distance;
	int honks;

	void (*honk)(struct Car *);
};

void Car_honk(struct Car *this) {
	printf("HONK! (%d)\n", this->honks); 
	this->honks++;
}

struct Car new_Car() {
	struct Car c;
	
	c.speed = 10;
	c.distance = 16583.71f;
	c.honks = 0;

	c.honk = &Car_honk;
	return c;
}

int main() {

	struct Car myCar = new_Car();

	printf("Speed %d\n", myCar.speed);
	printf("Distance %.2f\n", myCar.distance);
	myCar.honk(&myCar);
	myCar.honk(&myCar);
	myCar.honk(&myCar);
	return 0;
}