
#import <stdio.h>

extern int yyparse();
extern int yyerror(const char *);
extern int yylex();

int main(int argc, char *argv[])
{
	if (argc > 1)
	{
		freopen(argv[1],"r",stdin);
	}

	int result = yyparse();
	if (result == 0) {
		printf("Input is valid (%d).\n", result);
	}
	else {
		printf("Input is invalid (%d).\n", result);
	}
	return result;
}